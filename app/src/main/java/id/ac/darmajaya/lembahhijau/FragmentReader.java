package id.ac.darmajaya.lembahhijau;


import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import id.ac.darmajaya.lembahhijau.Database.DatabaseHelper;
import id.ac.darmajaya.lembahhijau.Model.ZooModel;


public class FragmentReader extends FragmentActivity {

    private TabLayout tablayout;
    private ViewPager viewPager;
    View view;
    String welcome ="Welcome";
    Bundle bundle = new Bundle();
    TextToSpeech tts;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragmentbut);


        IntentIntegrator integrator = new IntentIntegrator(FragmentReader.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.setOrientationLocked(true);
        integrator.initiateScan();


    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(getApplicationContext(), "Scanner dibatalkan",Toast.LENGTH_LONG).show();
                finish();
            } else {

                Toast.makeText(getApplicationContext(),"Scanned: " + result.getContents(),Toast.LENGTH_LONG).show();

                DatabaseHelper db = new DatabaseHelper(this);
                List<ZooModel> lembahhijau;
                lembahhijau = db.getzoolembahhijau();
                for (ZooModel p : lembahhijau) {
                    int id = p.getId();
                    String idbarcode = p.getIdbarcode();
                    System.out.println(idbarcode);

                    if (result.getContents().equals(idbarcode)){
                        bundle.putString("deskripsiid", p.getDeksripsiid());
                        bundle.putString("deskripsien", p.getDeskripsien());
                        bundle.putString("namahewanid", p.getNamahewanid());
                        bundle.putString("namahewanen", p.getNamahewanen());
                        bundle.putString("musikid", p.getMusikid());
                        bundle.putString("musiken", p.getMusiken());
                        fragmentdata();


                        ImageView iw= (ImageView)findViewById(R.id.imageView1);
                        iw.setImageBitmap(ImageViaAssets("images/"+p.getGambar()));


                    }

                    else
                    {
                        super.onActivityResult(requestCode, resultCode, data);

                    }
            }}
        }else {
            super.onActivityResult(requestCode, resultCode, data);
            finish();
        }
    }

    public void fragmentdata(){
        tablayout = (TabLayout) findViewById(R.id.tablayouttampil);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        FragmentID frag1 = new FragmentID();
        FragmentEN frag2 = new FragmentEN();

        frag1.setArguments(bundle);
        frag2.setArguments(bundle);

        adapter.AddFragment(frag1, "Indonesia");
        adapter.AddFragment(frag2, "English");
        viewPager.setAdapter(adapter);

        tablayout.setupWithViewPager(viewPager);
    }

    public Bitmap ImageViaAssets(String fileName){

        AssetManager assetmanager = getAssets();
        InputStream is = null;
        try{

            is = assetmanager.open(fileName);
        }catch(IOException e){
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        return bitmap;
    }


}

