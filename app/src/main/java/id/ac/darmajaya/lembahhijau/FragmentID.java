package id.ac.darmajaya.lembahhijau;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;


public class FragmentID extends Fragment implements TextToSpeech.OnInitListener {
    View view;
    //Variabel Global
    private Bundle bundle;
    String deskripsi, nmhwn;
    TextToSpeech tts;
    ImageButton mButtonSpeak;
    private int mstatus = 0;
    private MediaPlayer ring;


    public FragmentID(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_indonesia, container, false);

        TextView text = (TextView) view.findViewById(R.id.deksipsiid);//Find textview Id
        TextView namahewan = (TextView) view.findViewById(R.id.namahewanid);//Find textview Id

        bundle = this.getArguments();
        deskripsi = bundle.getString("deskripsiid");
        nmhwn = bundle.getString("namahewanid");
        text.setText(deskripsi);
        namahewan.setText(nmhwn);



        mButtonSpeak  = view.findViewById(R.id.btnspeak);
        mButtonSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mstatus == 0) {
                    ring= MediaPlayer.create(getActivity().getBaseContext(), Uri.parse("android.resource://"+view.getContext().getPackageName()+"/raw/"+bundle.getString("musiken")));
                    ring.start();
                    mstatus =1;
                    mButtonSpeak.setImageResource(R.drawable.ic_stop);

                }else{
                    ring.stop();
                    mButtonSpeak.setImageResource(R.drawable.ic_play);
                    mstatus =0;
                }
            }
        });


        return view;
    }


    @Override
    public void onInit(int status)
    {
        if (status == TextToSpeech.SUCCESS)
        {
            int result = tts.setLanguage(new Locale("id","ID"));
            tts.setSpeechRate(0);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language is not supported");
            } else {
                speakOut();
            }
        } else {
            Log.e("TTS", "Initilization Failed");
        }
    }

    @Override
    public void onDestroy()
    {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    private void speakOut()
    {
        tts.speak(deskripsi.toString(), TextToSpeech.QUEUE_FLUSH, null);
        Log.e("root ",  deskripsi.toString());
    }

}






