package id.ac.darmajaya.lembahhijau;

import android.media.MediaPlayer;
import android.net.Uri;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;


public class FragmentEN extends Fragment implements TextToSpeech.OnInitListener {

    View view;
    private Bundle bundle;
    String deskripsi, nmhwn;
    private TextToSpeech tts;
    ImageButton mButtonSpeak;
    private int mstatus = 0;

    private MediaPlayer ring;



    public FragmentEN(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_english, container, false);

        TextView text = (TextView) view.findViewById(R.id.deksipsien);//Find textview Id
        TextView namahewan = (TextView) view.findViewById(R.id.namahewanen);//Find textview Id

        bundle = this.getArguments();
        deskripsi = bundle.getString("deskripsien");
        nmhwn = bundle.getString("namahewanen");
        text.setText(deskripsi);
        namahewan.setText(nmhwn);

        mButtonSpeak  = view.findViewById(R.id.btnspeak);
        mButtonSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mstatus == 0) {

                    ring= MediaPlayer.create(getActivity().getBaseContext(), Uri.parse("android.resource://"+view.getContext().getPackageName()+"/raw/"+bundle.getString("musiken")));
                    ring.start();
                    mstatus =1;
                    mButtonSpeak.setImageResource(R.drawable.ic_stop);

                }else{
                    ring.stop();
                    mButtonSpeak.setImageResource(R.drawable.ic_play);
                    mstatus =0;
                }
            }
        });


        return view;
    }


    @Override
    public void onInit(int status)
    {
        if (status == TextToSpeech.SUCCESS)
        {
            int result = tts.setLanguage(Locale.US);
            tts.setSpeechRate(0);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language is not supported");
            } else {
                speakOut();
            }
        } else {
            Log.e("TTS", "Initilization Failed");
        }
    }

    @Override
    public void onDestroy()
    {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    private void speakOut()
    {
        tts.speak(deskripsi.toString(), TextToSpeech.QUEUE_FLUSH, null);
        Log.e("root ",  deskripsi.toString());
    }

}
