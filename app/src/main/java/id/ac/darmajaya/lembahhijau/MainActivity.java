package id.ac.darmajaya.lembahhijau;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import id.ac.darmajaya.lembahhijau.Database.DatabaseHelper;

public class MainActivity extends AppCompatActivity {
    private DatabaseHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        db = new DatabaseHelper(this);

        File database=getApplicationContext().getDatabasePath(DatabaseHelper.DBNAME);
        // Jika Database ada, maka copy data yang baru diupdate
        if(database.exists()){
            db.getReadableDatabase();
            db.close();
            if(copyDatabase(this)){
                Toast.makeText(getApplicationContext(),"Database Updated", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getApplicationContext(),"Database failed", Toast.LENGTH_LONG).show();
                return;
            }
        }
        //Jika database tidak ada, maka copy database dari assets
        if(!database.exists()){
            db.getReadableDatabase();
            db.close();
            if(copyDatabase(this)){
                Toast.makeText(getApplicationContext(),"Database Updated", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getApplicationContext(),"Database failed", Toast.LENGTH_LONG).show();
                return;
            }
        }

        //Button
/*    Button wayan = (Button)findViewById(R.id.Dancok);
        wayan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Basing.class);
                startActivity(intent);
            }
        });
*/
//End
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.scan_barcode:
                    Intent intent = new Intent(MainActivity.this, FragmentReader.class);
                    startActivity(intent);
                    return true;
            }
            return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.tentang_aplikasi) {
            Intent intent = new Intent(MainActivity.this, TentangAplikasi.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.petakebun){
            Intent intent = new Intent(MainActivity.this, KebunBinatang.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    private boolean copyDatabase(Context context) {
        try {
            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.w("Database", "Copy Success");
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
