package id.ac.darmajaya.lembahhijau.Model;

public class ZooModel {

    private int id;
    private String idbarcode;
    private String namahewanid;
    private String namahewanen;
    private String gambar;
    private String deksripsiid;
    private String deskripsien;
    private String musikid;
    private String musiken;

    public ZooModel(int id, String idbarcode, String namahewanid, String namahewanen, String gambar, String deksripsiid, String deskripsien, String musikid, String musiken) {
        this.id = id;
        this.idbarcode = idbarcode;
        this.namahewanid = namahewanid;
        this.namahewanen = namahewanen;
        this.gambar = gambar;
        this.deksripsiid = deksripsiid;
        this.deskripsien = deskripsien;
        this.musikid = musikid;
        this.musiken = musiken;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdbarcode() {
        return idbarcode;
    }

    public void setIdbarcode(String idbarcode) {
        this.idbarcode = idbarcode;
    }

    public String getNamahewanid() {
        return namahewanid;
    }

    public void setNamahewanid(String namahewanid) {
        this.namahewanid = namahewanid;
    }

    public String getNamahewanen() {
        return namahewanen;
    }

    public void setNamahewanen(String namahewanen) {
        this.namahewanen = namahewanen;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getDeksripsiid() {
        return deksripsiid;
    }

    public void setDeksripsiid(String deksripsiid) {
        this.deksripsiid = deksripsiid;
    }

    public String getDeskripsien() {
        return deskripsien;
    }

    public void setDeskripsien(String deskripsien) {
        this.deskripsien = deskripsien;
    }

    public String getMusikid() {
        return musikid;
    }

    public void setMusikid(String musikid) {
        this.musikid = musikid;
    }

    public String getMusiken() {
        return musiken;
    }

    public void setMusiken(String musiken) {
        this.musiken = musiken;
    }



}
